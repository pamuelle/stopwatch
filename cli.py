#!/usr/bin/env python3

import socket
import sys
import stopwatch

HOST = '127.0.0.1'  # The server's hostname or IP address
PORT = 33445        # The port used by the server

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
	command = sys.argv[1]
	command = command.encode()
	s.connect((HOST, PORT))
	s.sendall(command)
	s.close()

print('sent ', repr(command))