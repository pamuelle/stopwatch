import socket
from stopwatch import testFunc

# TCP Socket for communication
HOST = '127.0.0.1'  # Standard loopback interface address (localhost)
PORT = 33445        # Port to listen on (non-privileged ports are > 1023)
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))
s.listen()
while True:  
	# wait for a connection
	print ('waiting for a connection\n')
	connection, client_address = s.accept()

	try:
		# show who connected to us
		#print ('connection from', client_address)

		# receive the data in small chunks and print it
		while True:
			data = connection.recv(64)
			if data:
				if(data.decode() == "test"):
					testFunc()
				else:
					print("Command '" + data.decode() + "' not found.")
			else:
				# no more data -- quit the loop
				#print ("no more data.")
				break
	finally:
	    # Clean up the connection
	    connection.close()
