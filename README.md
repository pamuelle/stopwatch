== Introduction ==
This is a personal time tracking tool. The goal is to make measurements of some activities. E.g. studying a specific topic, going to the gym, commute, the point of time I go to sleep etc. As you see, studying, gym, commute have a start/end but going to bed doesn't. So we distinguish between measurements (pair of timestamps) and timepoints (single timestamp)

Example: (/foo represents a command, each command is a timestamp)

1. 7:00 - /getUp -> just one timestamp
2. 7:30 - /commute
3. 8:00 - /arrived -> /commute and /arrive make up one measurement
4. 8:00 - /calculus -> start learning calculus
5. 9:00 - /pause -> pause /calculus
6. 9:05 - /resume -> resume calculus
7. 11:00 - /pause -> pause /calculus
8. 11:05 - /resume -> resume calculus
9. 12:00 - /done -> end /calculus measurement

4-9 are one measurement, pauses are part of the measurement - for data analysis sake.

Now, /pause /resume /done are special commends which refere to the last issued measurement.

== Database Design ==
TODO: update design (get rid of timestamp table)
See: https://tinyurl.com/ycoalrsy
