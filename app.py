#!/usr/bin/python
from peewee import *
from stopwatch import *
import sys
from db import db

sp = Stopwatch()

try:
	command = sys.argv[1]
except:
	print('Provide a command.')
	sys.exit()

def init_db():
	db.create_tables([Pause, Category, Timepoint, Measurement])

def help():
	print('Help is coming...')

def list_types():
	timestamp_types = get_timestamp_types()
	for type in timestamp_types:
		print(type.title)

if command == 'init':
	init_db()

# Add category
elif command == 'add-category':
	try:
		sp.add_category(title=sys.argv[2])
		print("Added category" + sys.argv[2] + ".")
	except:
		print('Provide a category to add.')

# Add timepoint
elif command == 'add-timepoint':
	try:
		sp.add_timepoint(sys.argv[2])
		print("Added " + sys.argv[2] + " timepoint.")
	except:
		print('Provide a category for the timepoint.')

# Start a measurement
elif command == 'start-measurement':
	try:
		sp.start_measurement(sys.argv[2])
		print("Started measurement for " + sys.argv[2] + ".")
	except:
		print('Provide a category for your measurement.')

elif command == 'end-measurement':
	sp.end_measurement()
elif command == 'pause-measurement':
	sp.pause_measurement()
elif command == 'resume-measurement':
	sp.resume_measurement()
elif command == 'time':
	sp.get_current_measurement_duration()

elif command == 'help':
	help()
elif command == 'list-types':
	list_types()
elif command == 'add-type':
	try:
		add_timestamp_type(sys.argv[2])
	except:
		print('Provide a type to add.')
else:
	print('huh?')

