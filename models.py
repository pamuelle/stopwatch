from peewee import *
from db import db
import datetime as datetime

class BaseModel(Model):
    class Meta:
        database = db

class Category(BaseModel):
	title = CharField()
	comment = CharField(default="")

class Timepoint(BaseModel):
	timepoint = DateTimeField(default=datetime.datetime.now())
	category = ForeignKeyField(Category, backref='categories')
	comment = CharField(default="")

class Measurement(BaseModel):
	begin = DateTimeField(default=datetime.datetime.now())
	end = DateTimeField(null=True)
	category = ForeignKeyField(Category, backref='categories')
	comment = CharField(default="")

class Pause(BaseModel):
	begin = DateTimeField(default=datetime.datetime.now())
	end = DateTimeField(null=True)
	measurement = ForeignKeyField(Measurement, backref='measurements')
	comment = CharField(default="")