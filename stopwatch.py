from peewee import *
from models import *
import datetime as datetime

class Stopwatch:
	def add_measurement():
		pass

	def add_category(self, title):
		category = Category(title=title)
		category.save()

	def get_category(self, title):
		query = Category.select().where(Category.title == title)
		return query.get()

	def add_timepoint(self, category):
		query = Category.select().where(Category.title == category)
		category = query.get()
		timepoint = Timepoint(category=category)
		timepoint.save()

	def get_running_measurement(self):
		# Get last measurement (current measurement)
		query = Measurement.select().order_by(Measurement.id.desc())
		return query.get()

	def get_running_pause(self):
		# Get last pause (current pause)
		query = Pause.select().order_by(Pause.id.desc())
		return query.get()

	def get_pauses(self, measurement):
		query = Pause.select().join(Measurement).where(Pause.measurement == measurement)
		return query

	def start_measurement(self, category):
		category = self.get_category(title=category)
		measurement = Measurement(category=category)
		measurement.save()

	def pause_measurement(self):
		measurement = self.get_running_measurement()
		pause = Pause(measurement=measurement)
		pause.save()

	def resume_measurement(self):
		pause = self.get_running_pause()
		pause.end = datetime.datetime.now()
		pause.save()		

	def end_measurement(self):
		measurement = self.get_running_measurement()
		measurement.end = datetime.datetime.now()
		measurement.save()

	def get_current_measurement_duration(self):
		measurement = self.get_running_measurement()
		measurement_delta = measurement.end - measurement.begin;
		pauses = self.get_pauses(measurement)

		pause_delta = datetime.timedelta()

		for pause in pauses:
			pause_delta += pause.end - pause.begin

		delta = measurement_delta - pause_delta

		print(delta)